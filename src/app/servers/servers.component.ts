import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css']
})
export class ServersComponent implements OnInit {

  srvCreationStatus = "No server was added...";
  serverName="";
  serverCreated = false;
  servers =[];

  onCreateSrv(){
    this.serverCreated=true;
    this.servers.push('lol');
    this.srvCreationStatus = "Server was created! Name is "+this.serverName;
  }

  onUpdateSrvName(event: Event){
    this.serverName=(<HTMLInputElement>event.target).value;
  }

  constructor() { }

  ngOnInit() {
  }

}
