import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})
export class ServerComponent implements OnInit {

  isOnline: boolean = true;
  id: string;

  getStatus(): string{
    if(this.isOnline)
      return 'online';
    return'offline';
  }

  private getRand(floor, roof){
    return Math.floor(Math.random() * roof) + floor;
  }

  private randBool(): boolean{
    let v = this.getRand(0,3);
    return(v%2===0);
  }


  constructor() {
    this.id = this.getRand(0,10000)+'';
    this.isOnline=this.randBool();
  }

  getColor(){
    if(this.isOnline)
      return 'green';
    return'red';
  }

  ngOnInit() {
  }

}
